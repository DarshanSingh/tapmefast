//
//  ViewController.swift
//  TapMeFast
//
//  Created by AlienBrainz_mac4 on 04/12/18.
//  Copyright © 2018 Allien Brainz Software Pvt. Ltd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var time = 10
    var game = 0
    var gameScore = 0
    var timer = Timer()
    @IBOutlet weak var labeltime: UILabel!
    @IBOutlet weak var labelScore: UILabel!
    @IBOutlet weak var btnStartstop: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func btnstartstop(_ sender: Any) {
        if time == 10 {
            labeltime.text = String(time)
            labelScore.text = String(gameScore)
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
            btnStartstop.isEnabled = false
            btnStartstop.alpha = 0.5
        }
        if game == 1 {
            gameScore += 1
            labelScore.text = String(gameScore)
        }
        
        
    }
    
    @objc func updateTime (){
        time -= 1
        let randomnumber = arc4random_uniform(100)
        print(randomnumber)
        labeltime.text = String(time)
        game = 1
        btnStartstop.setTitle("Tap", for: UIControl.State.normal)
        btnStartstop.isEnabled = true
        btnStartstop.alpha = 1
        if time == 0 {
            timer.invalidate()
            btnStartstop.isEnabled = false
            btnStartstop.alpha = 0.5
            btnStartstop.setTitle("Restart", for: UIControl.State.normal)
            Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(restartGame), userInfo: nil, repeats: false)
        }
    }
    
    @objc func restartGame(){
        game = 0
        btnStartstop.isEnabled = true
        btnStartstop.alpha = 1
        gameScore = 0
        time = 10
    }

}



